import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ThemeModule } from '../../@theme/theme.module';

// Components
import { AuthComponent } from './auth.component';
import { AuthBlockComponent } from './auth-block/auth-block.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

// Services
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
import { UserService } from '../data/user.service';

@NgModule({
  declarations: [
    AuthComponent,
    AuthBlockComponent,
    LoginComponent,
    LogoutComponent,
  ],
  imports: [
    RouterModule,
    ThemeModule,
  ],
  providers: [
    AuthService,
    UserService,
    AuthGuard,
  ],
})
export class AuthModule {}
