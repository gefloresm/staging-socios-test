import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

// Services
import { AuthService } from '../services/auth.service';

interface User {
  name: string;
  email: string;
  password: string;
  rememberMe: boolean
}

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

  user: User = {
    name: '',
    email: '',
    password: '',
    rememberMe: false,
  };
  error: string;
  submitted = false;
  authService;
  router;
  authSubscribe: Subscription = new Subscription();

  static parameters = [AuthService, Router];
  constructor(
    _authService: AuthService,
    _router: Router,
   ) {
    this.authService = _authService;
    this.router = _router;
  }

  ngOnInit() {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.authSubscribe.unsubscribe();
  }

  login(form) {
    if (form.invalid) return;

    this.submitted = true;

    this.authSubscribe = this.authService
      .login(this.user)
      .subscribe((data) => {
        this.router.navigate(['/pages/dashboard']);
      },
      err => {
        this.submitted = false;
        this.error = err.statusText;
      });
  }

}
