import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { tap } from 'rxjs/operators';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    authService;
    router;

    static parameters = [AuthService, Router];
    constructor(authService: AuthService, router: Router) {
      this.authService = authService;
      this.router = router;
    }

    canActivate() {
      return this.authService.canActivate()
        .pipe(
          tap(authenticated => {
            if (!authenticated) {
              this.router.navigate(['/auth/login']);
            }
          }),
        );
    }
}
