import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of as observableOf, Observable, BehaviorSubject } from 'rxjs';

import { safeCb } from '../../utils/utils';
import { userRoles } from '../../../app.constants';

// Services
import { UserService } from '../../data/user.service';

// @flow
interface User {
  _id?: number;
  id?: string;
  name?: string;
  email?: string;
  role?: string;
}

@Injectable()
export class AuthService {
  http: HttpClient;
  _currentUser: User;
  userService: UserService;
  userRoles = userRoles || [];
  private nameToken = 'id_token';

  private currentUser$ = new BehaviorSubject<User>(this.currentUser);

  static parameters = [HttpClient, UserService];
  constructor(private _http: HttpClient, private _userService: UserService) {
    this.http = _http;
    this.userService = _userService;
  }

  /**
   * Check if userRole is >= role
   * @param {String} userRole - role of current user
   * @param {String} role - role to check against
   */
  static hasRole(userRole, role) {
    return userRoles.indexOf(userRole) >= userRoles.indexOf(role);
  }

  get currentUser() {
    return this._currentUser;
  }

  set currentUser(user) {
    this._currentUser = user;
    this.currentUser$.next(user);
  }

  /**
   * Authenticate user and save token
   *
   * @param  {Object}   user     - login info
   * @return {Observable}
   */
  login({email, password}): Observable<any> {
    return new Observable(observer => {
      this.http.post('/api/auth/local', {
        email,
        password,
      })
      .toPromise()
      .then((res: {token: string}) => {
        localStorage.setItem(this.nameToken, res.token);
        observer.next(res);
      })
      .catch(err => observer.error(err));
    });
  }

  /**
   * Delete access token and user info
   * @return {Observable}
   */
  logout(): void {
    localStorage.removeItem(this.nameToken);
    localStorage.removeItem('user');
    this.currentUser = null;
  }

  /**
   * Create a new user
   *
   * @param  {Object}   user     - user info
   * @param  {Function} callback - optional, function(error, user)
   * @return {Promise}
   */
  createUser(user, callback) {
    return this.userService.create(user).toPromise()
      .then((data: {token: string}) => {
        localStorage.setItem(this.nameToken, data.token);
        return this.userService.get().toPromise();
      })
      .then((_user: User) => {
        this.currentUser = _user;
        return safeCb(callback)(null, _user);
      })
      .catch(err => {
        this.logout();
        safeCb(callback)(err);
        return Promise.reject(err);
      });
  }

  /**
   * Change password
   *
   * @param  {String}   oldPassword
   * @param  {String}   newPassword
   * @param  {Function} [callback] - function(error, user)
   * @return {Promise}
   */
  changePassword(oldPassword, newPassword, callback) {
    return this.userService.changePassword({id: this.currentUser._id}, oldPassword, newPassword)
      .toPromise()
      .then(() => safeCb(callback)(null))
      .catch(err => safeCb(callback)(err));
  }

  /**
   * Gets all available info on a user
   *
   * @return {User}
   */
  getCurrentUser(): Observable<User> {
    return observableOf(this.currentUser);
  }

  /**
   * Gets all available info on a user
   *
   * @return {Observable}
   */
  onCurrentUser(): Observable<User> {
    return this.currentUser$.asObservable();
  }

  /**
   * Checks if user is logged in
   * @returns {Observable}
   */
  isLoggedIn(): Observable<boolean> {
    return observableOf(!!this.getToken());
  }

  /**
   * Checks if user is logged in and gets the user's data
   * @returns {Observable}
   */
  canActivate(): Observable<boolean> {
    return new Observable(observer => {
      if (this.getToken()) {
        this.userService
          .get()
          .subscribe(user => {
            this.currentUser = user;
            localStorage.setItem('user', JSON.stringify(user));
            observer.next(true);
          },
          () => {
            this.logout()
            observer.next(false);
          });
      } else {
        observer.next(false);
      }
    });
  }

  /**
   * Check if a user is an admin
   *
   * @param  {Function|*} [callback] - optional, function(is)
   * @return {Boolean}
   */
  isAdmin(callback?) {
    return this.currentUser.role === 'admin';
  }

  /**
   * Get auth token
   *
   * @return {String} - a token string used for authenticating
   */
  getToken(): string {
    return localStorage.getItem(this.nameToken);
  }
}
