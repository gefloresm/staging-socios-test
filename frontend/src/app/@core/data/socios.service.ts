import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class SociosService {

  static parameters = [HttpClient];
  constructor(private http: HttpClient) { }

  query(): Observable<any> {
    return this.http.get<any>('/api/administrarsocioss') as Observable<any>;
  }
  get(id: number): Observable<any> {
      return this.http.get<any>(`/api/administrarsocioss/${id}`) as Observable<any>;
  }
}
