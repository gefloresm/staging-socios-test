import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './user.service';
import { SolicitudesService } from './solicitudes.service';
import { SociosService } from './socios.service';
import { ContactosService } from './contactos.service';
import { SaldosymovimientosService } from './saldosymovimientos.service'
import { StateService } from './state.service';
import { SmartTableService } from './smart-table.service';

const SERVICES = [
  UserService,
  SolicitudesService,
  SociosService,
  ContactosService,
  SaldosymovimientosService,
  StateService,
  SmartTableService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
