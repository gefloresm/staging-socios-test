import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ContactosService {

  static parameters = [HttpClient];
  constructor(private http: HttpClient) { }

  query(): Observable<any> {
    return this.http.get<any>('/api/contactos') as Observable<any>;
  }
  get(contactos: any): Observable<any> {
      return this.http.get(`/api/contactos/${contactos.id}`) as Observable<any>;
  }
}
