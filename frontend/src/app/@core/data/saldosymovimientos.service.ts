import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class SaldosymovimientosService {

  static parameters = [HttpClient];
  constructor(private http: HttpClient) { }

  query(): Observable<any> {
    return this.http.get<any>('/api/movimientosycuentas') as Observable<any>;
  }
  get(id: number): Observable<any> {
      return this.http.get<any>(`/api/movimientosycuentas/${id}`) as Observable<any>;
  }
  save(data: any): Observable<any> {
      return this.http.post<any>('/api/movimientosycuentas', data) as Observable<any>;
  }
}
