// @flow
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

interface User {
  _id?: number;
  id?: string;
  name?: string;
  email?: string;
  role?: string;
}

@Injectable()
export class UserService {
    static parameters = [HttpClient];
    constructor(private http: HttpClient) {
        this.http = http;
    }

    query(): Observable<User[]> {
        return this.http.get('/api/users/') as Observable<User[]>;
    }
    get(user: User = {id: 'me'}): Observable<User> {
        return this.http.get(`/api/users/${user.id || user._id}`) as Observable<User>;
    }
    create(user: User) {
        return this.http.post('/api/users/', user);
    }
    changePassword(user, oldPassword, newPassword) {
        return this.http.put(`/api/users/${user.id || user._id}/password`, {oldPassword, newPassword});
    }
    remove(user) {
        return this.http.delete(`/api/users/${user.id || user._id}`)
            .pipe(map(() => user));
    }
}
