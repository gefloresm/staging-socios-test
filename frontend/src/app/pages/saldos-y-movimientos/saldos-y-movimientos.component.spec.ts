import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaldosYMovimientosComponent } from './saldos-y-movimientos.component';

describe('SaldosYMovimientosComponent', () => {
  let component: SaldosYMovimientosComponent;
  let fixture: ComponentFixture<SaldosYMovimientosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaldosYMovimientosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaldosYMovimientosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
