import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { LocalDataSource } from '../../../../node_modules/ng2-smart-table';
import { SmartTableService } from '../../@core/data/smart-table.service';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnDestroy {

  private alive = true;

  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      firstName: {
        title: 'Estado Solicitud',
        type: 'string',
      },
      lastName: {
        title: 'Razón Fallo',
        type: 'string',
      },
      username: {
        title: 'Días Solicitud',
        type: 'string',
      },
      email: {
        title: 'Horas hasta Asignar',
        type: 'string',
      },
      age: {
        title: 'Asignado por',
        type: 'number',
      },
      agente: {
        title: 'Notas Admin',
        type: 'string',
      },
      asignadoA: {
        title: 'Asignado a',
        type: 'string',
      },
      mismaIsapre: {
        title: 'Es de la misma Isapre',
        type: 'string',
      },
      emailMismaIsapre: {
        title: 'Email misma Isapre',
        type: 'string',
      },
      nombrePersona: {
        title: 'Nombre Persona',
        type: 'string',
      },
      rutPersona: {
        title: 'Rut Persona',
        type: 'string',
      },
      correoPersona: {
        title: 'Correo Persona',
        type: 'string',
      },
      telefonoPersona: {
        title: 'Teléfono Persona',
        type: 'string',
      },
      codigoPlan: {
        title: 'Código Plan',
        type: 'string',
      },
      codigoIsapre: {
        title: 'Código Isapre',
        type: 'string',
      },
      precioSolicitado: {
        title: 'Precio Solicitado',
        type: 'string',
      },
      fecha: {
        title: 'Fecha',
        type: 'string',
      },
      emailSocio: {
        title: 'Email Socio',
        type: 'string',
      },
      emailCliente: {
        title: 'Email Cliente',
        type: 'string',
      },
    },
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
    ],
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private themeService: NbThemeService, private service: SmartTableService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });
    const data = this.service.getData();
    this.source.load(data);
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que deseas eliminar este usuario?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}


