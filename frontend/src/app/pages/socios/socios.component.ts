import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';

// Components
import { DatatableComponent } from '@swimlane/ngx-datatable';

// Services
import { SociosService } from '../../@core/data/socios.service';

@Component({
  selector: 'ngx-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.scss'],
})
export class SociosComponent implements OnInit, OnDestroy {

  @ViewChild(DatatableComponent) table: DatatableComponent;
  private alive = true;

  rows: any[] = [];
  columns = [];
  total: number;
  temp: any[] = [];


  constructor(
    private sociosService: SociosService,
  ) { }

  ngOnInit() {
    this.sociosService.query().subscribe((socios: any) => {
      this.temp = [...socios];
      this.rows = socios;
      this.total = socios.length;
    });
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onDeleteConfirm(event): void {
    if (window.confirm('¿Seguro que deseas eliminar este usuario?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  filter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.temp.filter(function(d) {
      return (d.nombre_persona || '').toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rows = temp;
    this.table.offset = 0;
  }
}
