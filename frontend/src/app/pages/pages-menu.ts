import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Registro de Usuarios',
    icon: 'nb-plus-circled',
    link: '/pages/solicitudes',
    home: false,
  },
  {
    title: 'Lista de Solicitudes',
    icon: 'nb-list',
    link: '/pages/solicitudes',
    home: true,
  },
  {
    title: 'Socios',
    icon: 'nb-person',
    link: '/pages/socios',
    home: false,
  },
  {
    title: 'Contactos',
    icon: 'nb-keypad',
    link: '/pages/contactos',
    home: false,
  },
  {
    title: 'Saldos y Movimientos',
    icon: 'nb-bar-chart',
    link: '/pages/saldos-y-movimientos',
    home: false,
  },
  {
    title: 'Salir',
    icon: 'nb-close-circled',
    link: '/auth/logout',
    home: false,
  },
];
