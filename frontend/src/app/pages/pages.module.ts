import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { SociosComponent } from './socios/socios.component';
import { SmartTableService } from '../@core/data/smart-table.service';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { ContactosComponent } from './contactos/contactos.component';
import { SaldosYMovimientosComponent } from './saldos-y-movimientos/saldos-y-movimientos.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DetalleSolicitudComponent } from './solicitudes/detalle-solicitud/detalle-solicitud.component';
import { DetalleSocioComponent } from './socios/detalle-socio/detalle-socio.component';



const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    MiscellaneousModule,
    Ng2SmartTableModule,
    NgxDatatableModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    SociosComponent,
    SolicitudesComponent,
    ContactosComponent,
    SaldosYMovimientosComponent,
    DetalleSolicitudComponent,
    DetalleSocioComponent,
  ],
  providers: [
    SmartTableService,
  ],
})
export class PagesModule {
}
