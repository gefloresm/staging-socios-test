import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    <span class="created-by">Desarrollado por <b><a href="http://queplan.cl" target="_blank">QuePlan.cl</a></b> 2018</span>
  `,
})
export class FooterComponent {
}
