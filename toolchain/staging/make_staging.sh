#!/bin/bash 
/bin/echo -e '\e[1;32m[+] Eliminar y crear carpeta de staging\e[0m';
rm -rf staging
mkdir staging

cd backend
/bin/echo -e '\e[1;32m[+] Generando proyecto (npm install y gulp build)\e[0m';
npm install
npm install -g gulp
gulp build

mkdir dist/server/views/

if ( cp -rf server/views/* dist/server/views/ ) then
	/bin/echo -e '\e[1;32mPASS: Copiar archivos desde server/views a dist/server/views\e[0m';
else
	/bin/echo -e '\e[1;31mError: Copiar archivos desde server/views a dist/server/views\e[0m';
	exit 1
fi

/bin/echo -e '\e[1;32m[+] Mover carpeta server, package.json y .npmrc a carpeta de staging\e[0m';
if ( mv dist/client ../staging/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover carpeta server\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover carpeta server\e[0m';
	exit 1
fi
if ( mv dist/package.json ../staging/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover carpeta server\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover carpeta server\e[0m';
	exit 1
fi
if ( mv dist/server ../staging/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover carpeta server\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover carpeta server\e[0m';
	exit 1
fi
if ( mv .npmrc ../staging/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover archivo .npmrc\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover archivo .npmrc\e[0m';
	exit 1
fi
# Mover template de e-mail
mv incluircadavez/server/views/email/*.hbs server/views/email/

cd ..
if ( cp -rf toolchain/staging/manifest.yml staging/ ) then
	/bin/echo -e '\e[1;32mPASS: Copiar manifest del ambiente\e[0m';
else
	/bin/echo -e '\e[1;31mError: Copiar manifest.yml del ambiente\e[0m';
	exit 1
fi

/bin/echo -e '\e[1;32m[+] Limpiar el ambiente de staging\e[0m';
if ( mv staging/* . ) then
	/bin/echo -e '\e[1;32mPASS: Mover los archivos ya armados para su ejecucion\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover los archivos ya armados para su ejecucion\e[0m';
	exit 1
fi
if ( mv staging/.npmrc . ) then
	/bin/echo -e '\e[1;32mPASS: Mover TOKEN para NPM\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover TOKEN para NPM\e[0m';
	exit 1
fi

rm -rf .vscode backend .editorconfig staging
