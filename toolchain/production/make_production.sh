#!/bin/bash 
/bin/echo -e '\e[1;32m[+] Eliminar y crear carpeta de production\e[0m';
rm -rf production
mkdir production

cd backend
/bin/echo -e '\e[1;32m[+] Generando proyecto (npm install y gulp build)\e[0m';
npm install
npm install -g gulp
gulp build

mkdir dist/server/views/

if ( cp -rf server/views/* dist/server/views/ ) then
	/bin/echo -e '\e[1;32mPASS: Copiar archivos desde server/views a dist/server/views\e[0m';
else
	/bin/echo -e '\e[1;31mError: Copiar archivos desde server/views a dist/server/views\e[0m';
	exit 1
fi

/bin/echo -e '\e[1;32m[+] Mover carpeta server, package.json y .npmrc a carpeta de production\e[0m';
if ( mv dist/client ../production/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover carpeta server\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover carpeta server\e[0m';
	exit 1
fi
if ( mv dist/package.json ../production/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover carpeta server\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover carpeta server\e[0m';
	exit 1
fi
if ( mv dist/server ../production/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover carpeta server\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover carpeta server\e[0m';
	exit 1
fi
if ( mv .npmrc ../production/ ) then
	/bin/echo -e '\e[1;32mPASS: Mover archivo .npmrc\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover archivo .npmrc\e[0m';
	exit 1
fi
# Mover template de e-mail
mv incluircadavez/server/views/email/*.hbs server/views/email/

cd ..
if ( cp -rf toolchain/production/manifest.yml production/ ) then
	/bin/echo -e '\e[1;32mPASS: Copiar manifest del ambiente\e[0m';
else
	/bin/echo -e '\e[1;31mError: Copiar manifest.yml del ambiente\e[0m';
	exit 1
fi

/bin/echo -e '\e[1;32m[+] Limpiar el ambiente de production\e[0m';
if ( mv production/* . ) then
	/bin/echo -e '\e[1;32mPASS: Mover los archivos ya armados para su ejecucion\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover los archivos ya armados para su ejecucion\e[0m';
	exit 1
fi
if ( mv production/.npmrc . ) then
	/bin/echo -e '\e[1;32mPASS: Mover TOKEN para NPM\e[0m';
else
	/bin/echo -e '\e[1;31mError: Mover TOKEN para NPM\e[0m';
	exit 1
fi

rm -rf .vscode backend .editorconfig production