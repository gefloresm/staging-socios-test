'use strict';
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
module.exports = {
    port: process.env.OPENSHIFT_NODEJS_PORT
        || process.env.VCAP_APP_PORT
        || 8080,

    sequelize: {
        uri: 'postgres://u3ba12vem13u1e:peaa3fbccf3f3ec6186dbedf4ec0a23a7d6eea00e0b7ac270be6ecf0d11e25bba@ec2-35-169-142-214.compute-1.amazonaws.com:5432/dai50mnsbkb0hn',
        options: {
            database: 'dai50mnsbkb0hn',
            dialect: 'postgres',
            host: 'ec2-35-169-142-214.compute-1.amazonaws.com',
            port: 5432,
            username: 'u3ba12vem13u1e',
            password: 'peaa3fbccf3f3ec6186dbedf4ec0a23a7d6eea00e0b7ac270be6ecf0d11e25bba',
            dialectOptions: {
                ssl: {
                    require: true
                }
            },
            ssl: true,
            define: {
                timestamps: false
            }
        }
    }
};

  