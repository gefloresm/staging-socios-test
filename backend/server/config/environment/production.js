'use strict';
/*eslint no-process-env:0*/

// Production specific configuration
// =================================
module.exports = {
  // Server IP
  ip: process.env.OPENSHIFT_NODEJS_IP
    || process.env.ip
    || undefined,

  // Server port
  port: process.env.OPENSHIFT_NODEJS_PORT
    || process.env.PORT
    || 8080,

  sequelize: {
        uri: 'postgres://u35o94jes1prp0:p642a57eb6436a0df74ed09f1400a0ea79e500c35b5ad300a9500fd7ca1167e7d@ec2-34-196-233-196.compute-1.amazonaws.com:5432/dai50mnsbkb0hn',
        options: {
            //  logging: false,
            // storage: 'dev.sqlite',
            database: 'dai50mnsbkb0hn',
            dialect: 'postgres',
            host: 'ec2-34-196-233-196.compute-1.amazonaws.com',
            port: 5432,
            username: 'u35o94jes1prp0',
            password: 'p642a57eb6436a0df74ed09f1400a0ea79e500c35b5ad300a9500fd7ca1167e7d',
			dialectOptions:{
				ssl:{
					require:true
					}
			},
			ssl: true,
            define: {
                timestamps: false
            }
        }
    }
};
