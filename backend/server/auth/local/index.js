'use strict';

import express from 'express';
import passport from 'passport';
import {signToken} from '../auth.service';

var router = express.Router();

router.post('/', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    var error = err || info;
    if(error) {
      return res.status(401).json(error);
    }
    if(!user) {
      return res.status(404).json({message: 'Something went wrong, please try again.'});
    }

    let deviceId = req.headers['content-id']? req.headers['content-id']: '';
    let playerId = req.body.playerId? req.body.playerId : '';
    let appVersion = req.body.appVersion? req.body.appVersion: '';
    var token = signToken(user._id, user.role, deviceId, playerId, appVersion);
    res.json({ token });
  })(req, res, next);
});

export default router;
