/**
 * Main application routes
 */

'use strict';

import errors from './components/errors';
import path from 'path';

export default function(app) {
  // Insert routes below
  app.use('/api/globales', require('./api/globale'));
  app.use('/api/solicitarseguros', require('./api/solicitar_seguro'));
  
  app.use('/api/users', require('./api/user'));
  app.use('/auth', require('./auth').default);

  app.use('/api/recursos', require('./api/recurso'));
  app.use('/api/contactos', require('./api/contacto'));
  app.use('/api/movimientosycuentas', require('./api/movimientosycuenta'));
  app.use('/api/administrarsocioss', require('./api/administrarsocios'));
  app.use('/api/solicitarplans', require('./api/solicitarplan'));
  app.use('/api/things', require('./api/thing'));
  // Agreando configuraciones globales
  app.use('/api/configuracion', require('./api/configuracion'));
  // Notificaciones
  app.use('/api/notificacion', require('./api/notificaciones'));
  // Registro de movimiento de la aplicacion
  app.use('/api/log', require('./api/registro'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get((req, res) => {
      res.sendFile(path.resolve(`${app.get('appPath')}/index.html`));
    });
}
