/**
 * Sequelize initialization module
 */

'use strict';

import path from 'path';
import config from '../config/environment';
import Sequelize from 'sequelize';

var db = {
  Sequelize,
  sequelize: new Sequelize(config.sequelize.uri, config.sequelize.options)
};

// Insert models below
db.Globale = db.sequelize.import('../api/globale/globale.model');
db.Solicitarhistorico = db.sequelize.import('../api/solicitar_seguro/solicitar_historico.model');
db.Solicitarseguro = db.sequelize.import('../api/solicitar_seguro/solicitar_seguro.model');
db.Contacto = db.sequelize.import('../api/contacto/contacto.model');
db.Movimientosycuenta = db.sequelize.import('../api/movimientosycuenta/movimientosycuenta.model');
db.Administrarsocios = db.sequelize.import('../api/administrarsocios/administrarsocios.model');
db.Solicitarplan = db.sequelize.import('../api/solicitarplan/solicitarplan.model');
db.Thing = db.sequelize.import('../api/thing/thing.model');
db.User = db.sequelize.import('../api/user/user.model');

// configuraciones globales
db.Configuraciones = db.sequelize.import('../api/configuracion/configuracion.model');

// registros movil
db.Registros = db.sequelize.import('../api/registro/registro.model');

module.exports = db;
