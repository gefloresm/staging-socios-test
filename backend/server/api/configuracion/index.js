'use strict';

var express = require('express');
var controller = require('./configuracion.controller');

var router = express.Router();

router.get('/modificar', controller.update);
router.get('/consultar', controller.query);

module.exports = router;
