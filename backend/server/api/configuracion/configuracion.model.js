'use strict';

export default function (sequelize, DataTypes) {
    return sequelize.define('Configuraciones', {
        option_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        option_name: DataTypes.STRING,
        option_value: DataTypes.STRING
    }, {
        schema: 'constantes',
        tableName: 'configuraciones_globales',
        getterMethods: {
            getAll() {
                return {
                    option_id: this.option_id,
                    option_name: this.option_name,
                    option_value: this.option_value
                }
            }
        }
    });
}