'use strict';

import {Configuraciones} from '../../sqldb';

export function query(req, res){
    Configuraciones.find({
        where: {
          option_name: 'asignacion automatica'
        }
    }).then(data => {
        return res.status(200).json(data.getAll);
    })
}

export function update(req, res) {
    Configuraciones.find({
        where: { option_name: 'asignacion automatica' }
    }).then(data => {
        var configuracion_id = data.getAll.option_id;
        var estado = data.getAll.option_value == 'true' ? 'false' : 'true';
        Configuraciones.update({
            option_value: estado
        }, {
            returning: true,
            where: {
                option_id: configuracion_id
            }
        }).then(([ rowsUpdate, [updatedConfig] ]) => {
            return res.status(200).json(updatedConfig.getAll);
        });
    });
}
