'use strict';

import * as auth from '../../auth/auth.service';
var express = require('express');
var controller = require('./notificacion.controller');

var router = express.Router();

router.post('/bajo-saldo', auth.isAuthenticated(), controller.bajoSaldo);
router.post('/registrar', auth.isAuthenticated(), controller.registraUsuario);
router.post('/prospecto-nuevo', auth.isAuthenticated(), controller.prospectoNuevo);

module.exports = router;
