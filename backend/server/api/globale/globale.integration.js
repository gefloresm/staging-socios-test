'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newGlobale;

describe('Globale API:', function() {
  describe('GET /api/globales', function() {
    var globales;

    beforeEach(function(done) {
      request(app)
        .get('/api/globales')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          globales = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(globales).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/globales', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/globales')
        .send({
          name: 'New Globale',
          info: 'This is the brand new globale!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newGlobale = res.body;
          done();
        });
    });

    it('should respond with the newly created globale', function() {
      expect(newGlobale.name).to.equal('New Globale');
      expect(newGlobale.info).to.equal('This is the brand new globale!!!');
    });
  });

  describe('GET /api/globales/:id', function() {
    var globale;

    beforeEach(function(done) {
      request(app)
        .get(`/api/globales/${newGlobale._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          globale = res.body;
          done();
        });
    });

    afterEach(function() {
      globale = {};
    });

    it('should respond with the requested globale', function() {
      expect(globale.name).to.equal('New Globale');
      expect(globale.info).to.equal('This is the brand new globale!!!');
    });
  });

  describe('PUT /api/globales/:id', function() {
    var updatedGlobale;

    beforeEach(function(done) {
      request(app)
        .put(`/api/globales/${newGlobale._id}`)
        .send({
          name: 'Updated Globale',
          info: 'This is the updated globale!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedGlobale = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedGlobale = {};
    });

    it('should respond with the updated globale', function() {
      expect(updatedGlobale.name).to.equal('Updated Globale');
      expect(updatedGlobale.info).to.equal('This is the updated globale!!!');
    });

    it('should respond with the updated globale on a subsequent GET', function(done) {
      request(app)
        .get(`/api/globales/${newGlobale._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let globale = res.body;

          expect(globale.name).to.equal('Updated Globale');
          expect(globale.info).to.equal('This is the updated globale!!!');

          done();
        });
    });
  });

  describe('PATCH /api/globales/:id', function() {
    var patchedGlobale;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/globales/${newGlobale._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Globale' },
          { op: 'replace', path: '/info', value: 'This is the patched globale!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedGlobale = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedGlobale = {};
    });

    it('should respond with the patched globale', function() {
      expect(patchedGlobale.name).to.equal('Patched Globale');
      expect(patchedGlobale.info).to.equal('This is the patched globale!!!');
    });
  });

  describe('DELETE /api/globales/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/globales/${newGlobale._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when globale does not exist', function(done) {
      request(app)
        .delete(`/api/globales/${newGlobale._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
