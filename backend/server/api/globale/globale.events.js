/**
 * Globale model events
 */

'use strict';

import {EventEmitter} from 'events';
var Globale = require('../../sqldb').Globale;
var GlobaleEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
GlobaleEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Globale) {
  for(var e in events) {
    let event = events[e];
    Globale.hook(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc, options, done) {
    GlobaleEvents.emit(event + ':' + doc._id, doc);
    GlobaleEvents.emit(event, doc);
    done(null);
  };
}

registerEvents(Globale);
export default GlobaleEvents;
