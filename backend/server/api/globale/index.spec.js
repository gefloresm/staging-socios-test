'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var globaleCtrlStub = {
  index: 'globaleCtrl.index',
  show: 'globaleCtrl.show',
  create: 'globaleCtrl.create',
  upsert: 'globaleCtrl.upsert',
  patch: 'globaleCtrl.patch',
  destroy: 'globaleCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var globaleIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './globale.controller': globaleCtrlStub
});

describe('Globale API Router:', function() {
  it('should return an express router instance', function() {
    expect(globaleIndex).to.equal(routerStub);
  });

  describe('GET /api/globales', function() {
    it('should route to globale.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'globaleCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/globales/:id', function() {
    it('should route to globale.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'globaleCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/globales', function() {
    it('should route to globale.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'globaleCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/globales/:id', function() {
    it('should route to globale.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'globaleCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/globales/:id', function() {
    it('should route to globale.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'globaleCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/globales/:id', function() {
    it('should route to globale.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'globaleCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
