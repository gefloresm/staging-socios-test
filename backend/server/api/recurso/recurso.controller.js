'use strict';

var http = require('http');

function obteneruf() {
  var request = http.get("http://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=0e0c1492d3082827707e37eac60b2eef9bcb3f79&formato=json", function (response) {
    var body = "";

    response.on("data", function (chunk) {
      body += chunk;
    });

    response.on("end", function () {
      if (response.statusCode === 200) {
        try {
          var datos = JSON.parse(body);
          UFDelDia = Number(datos.UFs[0].Valor.replace(".", "").replace(",", "."));
          fechaDeLaUF = datos.UFs[0].Fecha;
          console.log("Valor de la UF Asignada: ", UFDelDia);
          console.log("Tipo: ", typeof UFDelDia);
          console.log("Día ", fechaDeLaUF);
          return;
        } catch (error) {
          console.error(error.message);
          return;
        }
      }
    });
  });

  // Connection Error
  request.on("error", function (error) {
    console.error(error);
    return;
  });
}
var fechaDeLaUF = false;
var UFDelDia = 26635.35;
obteneruf();
setInterval(obteneruf, 4 * 60 * 60 * 1000);

// Gets a list of Recursos
export function index(req, res) {
  res.status(200).json({
    valor: UFDelDia,
    dia: fechaDeLaUF
  });
}
