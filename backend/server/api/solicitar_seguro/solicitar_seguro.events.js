/**
 * Solicitarseguro model events
 */

'use strict';

import {EventEmitter} from 'events';
var Solicitarseguro = require('../../sqldb').Solicitarseguro;
var SolicitarseguroEvents = new EventEmitter();
const pg = require('pg');
  
console.log('LISTEN solicitar_seguroEvent');
const client = new pg.Client();
client.query('solicitar_seguroEvent');


// Set max event listeners (0 == unlimited)
SolicitarseguroEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'create',
  afterUpdate: 'update',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Solicitarseguro) {
  for(var e in events) {
    let event = events[e];
    console.log(event);
    Solicitarseguro.hook(e, emitEvent(event));
  }
}

function emitEvent(event) {
  client.query(`NOTIFY solicitar_seguroEvent, 'solicitar_seguroEvent'`);
  return function(doc, options, done) {
    SolicitarseguroEvents.emit(event + ':' + doc._id, doc);
    console.log("SolicitarseguroEvents.emit=  event + ':' + doc._id, doc");
    console.log(event + ':' + doc._id, doc);
    SolicitarseguroEvents.emit(event, doc);
    done(null);
  };
}

registerEvents(Solicitarseguro);
export default SolicitarseguroEvents;
