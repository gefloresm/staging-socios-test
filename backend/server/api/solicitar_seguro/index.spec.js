'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var solicitarseguroCtrlStub = {
  index: 'solicitarseguroCtrl.index',
  show: 'solicitarseguroCtrl.show',
  create: 'solicitarseguroCtrl.create',
  upsert: 'solicitarseguroCtrl.upsert',
  patch: 'solicitarseguroCtrl.patch',
  destroy: 'solicitarseguroCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var solicitarseguroIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './solicitarseguro.controller': solicitarseguroCtrlStub
});

describe('Solicitarseguro API Router:', function() {
  it('should return an express router instance', function() {
    expect(solicitarseguroIndex).to.equal(routerStub);
  });

  describe('GET /api/solicitarseguros', function() {
    it('should route to solicitarseguro.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'solicitarseguroCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/solicitarseguros/:id', function() {
    it('should route to solicitarseguro.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'solicitarseguroCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/solicitarseguros', function() {
    it('should route to solicitarseguro.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'solicitarseguroCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/solicitarseguros/:id', function() {
    it('should route to solicitarseguro.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'solicitarseguroCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/solicitarseguros/:id', function() {
    it('should route to solicitarseguro.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'solicitarseguroCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/solicitarseguros/:id', function() {
    it('should route to solicitarseguro.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'solicitarseguroCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
