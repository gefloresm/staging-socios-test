'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('solicitar_seguro', {
    id_solicitud: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    rut_persona: DataTypes.INTEGER,
    correo_persona: DataTypes.STRING,
    nombre_persona: DataTypes.STRING,
    telefono_persona: DataTypes.STRING,
    estado_solicitud: DataTypes.INTEGER,
    razon_fallo: DataTypes.INTEGER,
    asignado_por: DataTypes.INTEGER,
    notas_admin: DataTypes.STRING,
    asignado_a: DataTypes.STRING,
    array_persona: DataTypes.JSON,
    array_seguro: DataTypes.JSON,
    id_seguro: DataTypes.INTEGER,
    precio_solicitado: DataTypes.FLOAT,
    fecha: DataTypes.DATE,
    ip: DataTypes.STRING,
    email_notificacion_cliente: DataTypes.STRING,
    email_notificacion_ejecutivo: DataTypes.STRING,
    email_asignacion_cliente: DataTypes.STRING,
    email_asignacion_ejecutivo: DataTypes.STRING,
    region: DataTypes.INTEGER
  },
  {
    freezeTableName: true,
    schema: 'sa'
  });
}
