'use strict';

export default function(sequelize, DataTypes) {
  return sequelize.define('solicitar_historico', { 
    id_modificacion: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    id_solicitud: DataTypes.INTEGER,
    columna_modificada: DataTypes.STRING,
    dato_anterior: DataTypes.STRING,
    dato_nuevo: DataTypes.STRING,
    ip_modificacion: DataTypes.STRING,
    id_usuario_modificador: DataTypes.INTEGER,
    fecha_modificacion: DataTypes.DATE
  },
  {
    freezeTableName: true,
    schema: 'sa'
  });
}
