'use strict';

var express = require('express');
var controller = require('./registro.controller');

var router = express.Router();

router.post('/registrar', controller.registrar);

module.exports = router;
