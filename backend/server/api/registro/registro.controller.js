'use strict';

import { Registros } from '../../sqldb';

export function registrar(req, res) {
    let ip = (req.headers['cf-connecting-ip'] ||
     req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress).split(",")[0];

    return Registros.create({
        tipo_peticion: req.body.info.type,
        endpoint: req.body.info.endpoint,
        data: JSON.stringify(req.body.info),
        id_usuario: req.id_usuario,
        id_player: req.playerId,
        id_device: req.id_device,
        ip: ip,
        agente_usuario: req.headers['user-agent'],
        info_app: req.body.appVersion
    }).then(() => {
        res.json({"estado": "ok"});
    })
}
