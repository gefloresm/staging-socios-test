'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var movimientosycuentaCtrlStub = {
  index: 'movimientosycuentaCtrl.index',
  show: 'movimientosycuentaCtrl.show',
  create: 'movimientosycuentaCtrl.create',
  upsert: 'movimientosycuentaCtrl.upsert',
  patch: 'movimientosycuentaCtrl.patch',
  destroy: 'movimientosycuentaCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var movimientosycuentaIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './movimientosycuenta.controller': movimientosycuentaCtrlStub
});

describe('Movimientosycuenta API Router:', function() {
  it('should return an express router instance', function() {
    expect(movimientosycuentaIndex).to.equal(routerStub);
  });

  describe('GET /api/movimientosycuentas', function() {
    it('should route to movimientosycuenta.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'movimientosycuentaCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/movimientosycuentas/:id', function() {
    it('should route to movimientosycuenta.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'movimientosycuentaCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/movimientosycuentas', function() {
    it('should route to movimientosycuenta.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'movimientosycuentaCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/movimientosycuentas/:id', function() {
    it('should route to movimientosycuenta.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'movimientosycuentaCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/movimientosycuentas/:id', function() {
    it('should route to movimientosycuenta.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'movimientosycuentaCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/movimientosycuentas/:id', function() {
    it('should route to movimientosycuenta.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'movimientosycuentaCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
