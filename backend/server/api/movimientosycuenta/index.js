'use strict';

var express = require('express');
var controller = require('./movimientosycuenta.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.hasRole('admin'), controller.index);
router.get('/mismovimientos', auth.isAuthenticated(), controller.mismovimientos);
router.get('/misaldo', auth.isAuthenticated(), controller.misaldo);
router.post('/', auth.hasRole('admin'), controller.create);

module.exports = router;
