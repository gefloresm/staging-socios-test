'use strict';

export default function (sequelize, DataTypes) {
    return sequelize.define('Movimientosycuenta', {
        _id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        id_socio: DataTypes.INTEGER,
        monto: DataTypes.INTEGER,
        tipo_de_movimiento: DataTypes.INTEGER,
        descripcion: DataTypes.TEXT,
        id_de_solicitud: DataTypes.STRING,
        fecha_movimiento: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        originado_por_id: DataTypes.INTEGER
    });
}