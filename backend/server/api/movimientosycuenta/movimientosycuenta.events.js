/**
 * Movimientosycuenta model events
 */

'use strict';

import {EventEmitter} from 'events';
var Movimientosycuenta = require('../../sqldb').Movimientosycuenta;
var MovimientosycuentaEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
MovimientosycuentaEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Movimientosycuenta) {
  for(var e in events) {
    let event = events[e];
    Movimientosycuenta.hook(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc, options, done) {
    MovimientosycuentaEvents.emit(event + ':' + doc._id, doc);
    MovimientosycuentaEvents.emit(event, doc);
    done(null);
  };
}

registerEvents(Movimientosycuenta);
export default MovimientosycuentaEvents;
