'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newMovimientosycuenta;

describe('Movimientosycuenta API:', function() {
  describe('GET /api/movimientosycuentas', function() {
    var movimientosycuentas;

    beforeEach(function(done) {
      request(app)
        .get('/api/movimientosycuentas')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          movimientosycuentas = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      expect(movimientosycuentas).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/movimientosycuentas', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/movimientosycuentas')
        .send({
          name: 'New Movimientosycuenta',
          info: 'This is the brand new movimientosycuenta!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newMovimientosycuenta = res.body;
          done();
        });
    });

    it('should respond with the newly created movimientosycuenta', function() {
      expect(newMovimientosycuenta.name).to.equal('New Movimientosycuenta');
      expect(newMovimientosycuenta.info).to.equal('This is the brand new movimientosycuenta!!!');
    });
  });

  describe('GET /api/movimientosycuentas/:id', function() {
    var movimientosycuenta;

    beforeEach(function(done) {
      request(app)
        .get(`/api/movimientosycuentas/${newMovimientosycuenta._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          movimientosycuenta = res.body;
          done();
        });
    });

    afterEach(function() {
      movimientosycuenta = {};
    });

    it('should respond with the requested movimientosycuenta', function() {
      expect(movimientosycuenta.name).to.equal('New Movimientosycuenta');
      expect(movimientosycuenta.info).to.equal('This is the brand new movimientosycuenta!!!');
    });
  });

  describe('PUT /api/movimientosycuentas/:id', function() {
    var updatedMovimientosycuenta;

    beforeEach(function(done) {
      request(app)
        .put(`/api/movimientosycuentas/${newMovimientosycuenta._id}`)
        .send({
          name: 'Updated Movimientosycuenta',
          info: 'This is the updated movimientosycuenta!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedMovimientosycuenta = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedMovimientosycuenta = {};
    });

    it('should respond with the updated movimientosycuenta', function() {
      expect(updatedMovimientosycuenta.name).to.equal('Updated Movimientosycuenta');
      expect(updatedMovimientosycuenta.info).to.equal('This is the updated movimientosycuenta!!!');
    });

    it('should respond with the updated movimientosycuenta on a subsequent GET', function(done) {
      request(app)
        .get(`/api/movimientosycuentas/${newMovimientosycuenta._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let movimientosycuenta = res.body;

          expect(movimientosycuenta.name).to.equal('Updated Movimientosycuenta');
          expect(movimientosycuenta.info).to.equal('This is the updated movimientosycuenta!!!');

          done();
        });
    });
  });

  describe('PATCH /api/movimientosycuentas/:id', function() {
    var patchedMovimientosycuenta;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/movimientosycuentas/${newMovimientosycuenta._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Movimientosycuenta' },
          { op: 'replace', path: '/info', value: 'This is the patched movimientosycuenta!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedMovimientosycuenta = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedMovimientosycuenta = {};
    });

    it('should respond with the patched movimientosycuenta', function() {
      expect(patchedMovimientosycuenta.name).to.equal('Patched Movimientosycuenta');
      expect(patchedMovimientosycuenta.info).to.equal('This is the patched movimientosycuenta!!!');
    });
  });

  describe('DELETE /api/movimientosycuentas/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/movimientosycuentas/${newMovimientosycuenta._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when movimientosycuenta does not exist', function(done) {
      request(app)
        .delete(`/api/movimientosycuentas/${newMovimientosycuenta._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
