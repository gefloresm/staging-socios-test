'use strict';

import jsonpatch from 'fast-json-patch';
import {Movimientosycuenta} from '../../sqldb';
import seqconsult from '../../sqldb';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function(entity) {
    if(entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function patchUpdates(patches) {
  return function(entity) {
    try {
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch(err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function(entity) {
    if(entity) {
      return entity.destroy()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function(entity) {
    if(!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function(err) {
    res.status(statusCode).send(err);
  };
}

// Gets a list of Movimientosycuentas
export function index(req, res) {
  return Movimientosycuenta.findAll({order:[['_id', 'DESC']]})
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Gets a single Movimientosycuenta from the DB
export function show(req, res) {
  return Movimientosycuenta.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Creates a new Movimientosycuenta in the DB
export function create(req, res) {
    req.body.originado_por_id = req.user._id;
    req.body.monto = parseInt(req.body.monto);
    seqconsult.sequelize.query('UPDATE "Administrarsocios" SET saldo = saldo + ? WHERE _id = ?', {
        replacements: [req.body.monto, req.body.id_socio],
        type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
        Movimientosycuenta.create(req.body)
            .then(respondWithResult(res, 201))
            .catch(handleError(res))
    }).catch(handleError(res));
}

// Upserts the given Movimientosycuenta in the DB at the specified ID
export function upsert(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }

  return Movimientosycuenta.upsert(req.body, {
    where: {
      _id: req.params.id
    }
  })
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Updates an existing Movimientosycuenta in the DB
export function patch(req, res) {
  if(req.body._id) {
    delete req.body._id;
  }
  return Movimientosycuenta.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Movimientosycuenta from the DB
export function destroy(req, res) {
  return Movimientosycuenta.find({
    where: {
      _id: req.params.id
    }
  })
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}

export function mismovimientos(req, res) {
  var ipcliente = (req.headers['cf-connecting-ip'] ||
  req.headers['x-forwarded-for'] ||
  req.connection.remoteAddress ||
  req.socket.remoteAddress ||
  req.connection.socket.remoteAddress).split(",")[0];
    seqconsult.sequelize.query('SELECT *, (SELECT array_to_json(array_agg(row_to_json(solicitud))) FROM (SELECT * FROM "Solicitarplans"  WHERE "Solicitarplans"._id = M.id_de_solicitud::integer LIMIT 20) solicitud) AS solicitud FROM "Movimientosycuenta" M WHERE id_socio = (SELECT _id FROM "Administrarsocios" WHERE id_de_usuario = ? ORDER BY _id DESC LIMIT 1) ORDER BY _id DESC', {
        replacements: [req.user._id],
        type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
        console.log("el siguiente usuario llegó a sus movimientos " + req.user._id + ' ip: ' + ipcliente);
        res.json(results);
    })

}

export function misaldo(req, res) {
  var ipcliente = (req.headers['cf-connecting-ip'] ||
  req.headers['x-forwarded-for'] ||
  req.connection.remoteAddress ||
  req.socket.remoteAddress ||
  req.connection.socket.remoteAddress).split(",")[0];

    seqconsult.sequelize.query('SELECT saldo FROM "Administrarsocios" WHERE id_de_usuario = ? LIMIT 1', {
        replacements: [req.user._id],
        type: seqconsult.sequelize.QueryTypes.SELECT
    }).then(function (results) {
        console.log("el siguiente usuario llegó a su saldo " + req.user._id + ' ip: ' + ipcliente);
        res.json(results);
    })

}
