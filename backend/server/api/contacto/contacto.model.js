'use strict';

export default function (sequelize, DataTypes) {
    return sequelize.define('Contacto', {
        _id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        nombre: DataTypes.STRING,
        email: DataTypes.STRING,
        mensaje: DataTypes.TEXT,
        fecha: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        telefono: DataTypes.STRING,
        datosingresados: DataTypes.JSON,
        ip: DataTypes.STRING,
        emailsocio: DataTypes.TEXT,
        emailcliente: DataTypes.TEXT,
        estado_contacto: DataTypes.INTEGER,
        origen: DataTypes.INTEGER,
        notas_admin: DataTypes.TEXT,
        asignado_a: DataTypes.INTEGER
    });
}