/**
 * Contacto model events
 */

'use strict';

import {EventEmitter} from 'events';
var Contacto = require('../../sqldb').Contacto;
var ContactoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ContactoEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Contacto) {
  for(var e in events) {
    let event = events[e];
    Contacto.hook(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc, options, done) {
    ContactoEvents.emit(event + ':' + doc._id, doc);
    ContactoEvents.emit(event, doc);
    done(null);
  };
}

registerEvents(Contacto);
export default ContactoEvents;
