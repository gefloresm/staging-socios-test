'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var contactoCtrlStub = {
  index: 'contactoCtrl.index',
  show: 'contactoCtrl.show',
  create: 'contactoCtrl.create',
  upsert: 'contactoCtrl.upsert',
  patch: 'contactoCtrl.patch',
  destroy: 'contactoCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var contactoIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './contacto.controller': contactoCtrlStub
});

describe('Contacto API Router:', function() {
  it('should return an express router instance', function() {
    expect(contactoIndex).to.equal(routerStub);
  });

  describe('GET /api/contactos', function() {
    it('should route to contacto.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'contactoCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/contactos/:id', function() {
    it('should route to contacto.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'contactoCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/contactos', function() {
    it('should route to contacto.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'contactoCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/contactos/:id', function() {
    it('should route to contacto.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'contactoCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/contactos/:id', function() {
    it('should route to contacto.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'contactoCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/contactos/:id', function() {
    it('should route to contacto.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'contactoCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
