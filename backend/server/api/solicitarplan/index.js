'use strict';

var express = require('express');
var controller = require('./solicitarplan.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/missolicitudes', auth.isAuthenticated(), controller.solicitudesdelsocio);
router.get('/', auth.isAuthenticated(), controller.index);
router.post('/emailasignacion', auth.isAuthenticated(), controller.asignarSocioEmail);
router.post('/socioemailasignacion', auth.isAuthenticated(), controller.enviarEmailaSocio);
router.patch('/:id', auth.isAuthenticated(), controller.patch);

module.exports = router;
