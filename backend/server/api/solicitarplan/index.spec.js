'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var solicitarplanCtrlStub = {
  index: 'solicitarplanCtrl.index',
  show: 'solicitarplanCtrl.show',
  create: 'solicitarplanCtrl.create',
  upsert: 'solicitarplanCtrl.upsert',
  patch: 'solicitarplanCtrl.patch',
  destroy: 'solicitarplanCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var solicitarplanIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './solicitarplan.controller': solicitarplanCtrlStub
});

describe('Solicitarplan API Router:', function() {
  it('should return an express router instance', function() {
    expect(solicitarplanIndex).to.equal(routerStub);
  });

  describe('GET /api/solicitarplans', function() {
    it('should route to solicitarplan.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'solicitarplanCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/solicitarplans/:id', function() {
    it('should route to solicitarplan.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'solicitarplanCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/solicitarplans', function() {
    it('should route to solicitarplan.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'solicitarplanCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/solicitarplans/:id', function() {
    it('should route to solicitarplan.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'solicitarplanCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/solicitarplans/:id', function() {
    it('should route to solicitarplan.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'solicitarplanCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/solicitarplans/:id', function() {
    it('should route to solicitarplan.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'solicitarplanCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
