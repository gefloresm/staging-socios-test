'use strict';

/* globals sinon, describe, expect, it */

var proxyquire = require('proxyquire').noPreserveCache();

var administrarsociosCtrlStub = {
  index: 'administrarsociosCtrl.index',
  show: 'administrarsociosCtrl.show',
  create: 'administrarsociosCtrl.create',
  upsert: 'administrarsociosCtrl.upsert',
  patch: 'administrarsociosCtrl.patch',
  destroy: 'administrarsociosCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var administrarsociosIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './administrarsocios.controller': administrarsociosCtrlStub
});

describe('Administrarsocios API Router:', function() {
  it('should return an express router instance', function() {
    expect(administrarsociosIndex).to.equal(routerStub);
  });

  describe('GET /api/administrarsocioss', function() {
    it('should route to administrarsocios.controller.index', function() {
      expect(routerStub.get
        .withArgs('/', 'administrarsociosCtrl.index')
        ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/administrarsocioss/:id', function() {
    it('should route to administrarsocios.controller.show', function() {
      expect(routerStub.get
        .withArgs('/:id', 'administrarsociosCtrl.show')
        ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/administrarsocioss', function() {
    it('should route to administrarsocios.controller.create', function() {
      expect(routerStub.post
        .withArgs('/', 'administrarsociosCtrl.create')
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/administrarsocioss/:id', function() {
    it('should route to administrarsocios.controller.upsert', function() {
      expect(routerStub.put
        .withArgs('/:id', 'administrarsociosCtrl.upsert')
        ).to.have.been.calledOnce;
    });
  });

  describe('PATCH /api/administrarsocioss/:id', function() {
    it('should route to administrarsocios.controller.patch', function() {
      expect(routerStub.patch
        .withArgs('/:id', 'administrarsociosCtrl.patch')
        ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/administrarsocioss/:id', function() {
    it('should route to administrarsocios.controller.destroy', function() {
      expect(routerStub.delete
        .withArgs('/:id', 'administrarsociosCtrl.destroy')
        ).to.have.been.calledOnce;
    });
  });
});
