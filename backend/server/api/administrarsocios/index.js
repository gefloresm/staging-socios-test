'use strict';

var express = require('express');
var controller = require('./administrarsocios.controller');
import * as auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.patch('/:id', auth.isAuthenticated(), controller.patch);
router.post('/asignar', auth.isAuthenticated(), controller.getAsignar);
router.get('/tasa-cierre', auth.hasRole('admin'), controller.tasaCierre);
router.post('/tasa-cierre-movil', auth.isAuthenticated(), controller.tasaCierreMovil);
router.post('/tasa-cierre-fija', auth.isAuthenticated(), controller.tasaCierreFija);
router.put('/:id/password', auth.hasRole('admin'), controller.changePassword);

module.exports = router;
