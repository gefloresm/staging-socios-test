/**
 * Administrarsocios model events
 */

'use strict';

import {EventEmitter} from 'events';
var Administrarsocios = require('../../sqldb').Administrarsocios;
var AdministrarsociosEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
AdministrarsociosEvents.setMaxListeners(0);

// Model events
var events = {
  afterCreate: 'save',
  afterUpdate: 'save',
  afterDestroy: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Administrarsocios) {
  for(var e in events) {
    let event = events[e];
    Administrarsocios.hook(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc, options, done) {
    AdministrarsociosEvents.emit(event + ':' + doc._id, doc);
    AdministrarsociosEvents.emit(event, doc);
    done(null);
  };
}

registerEvents(Administrarsocios);
export default AdministrarsociosEvents;
