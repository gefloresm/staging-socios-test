'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './missolicitudes.routes';

export class MissolicitudesComponent {
    /*@ngInject*/
    constructor($scope, $http, $uibModal) {
        'ngInject';
        $http.get('/api/solicitarplans/missolicitudes')
            .then(function (data) {
                $scope.gridOptions.data = data.data;
                console.log(data);
            });
        $scope.abrirmodalsolicitud = function (row) {
            $uibModal.open({
                animation: true,
                template: require('./vermodalsolicitud.html'),
                size: 'lg',
                controller: function ($scope, $uibModalInstance, row) {
                    // var $ctrl = this;
                    $scope.entityfila = angular.copy(row.entity);
                    console.log(row);
                    $scope.razones = {
                        availableOptions: [
                            {
                                id: '1',
                                name: 'No contrata plan solicitado'
                            },
                            {
                                id: '2',
                                name: 'Cliente quiere otra isapre/ Cierre en '
                            },
                            {
                                id: '3',
                                name: 'Plan solicitado no está en comercialización y no contrata otro plan'
                            },
                            {
                                id: '4',
                                name: 'Cliente con preexistencia'
                            },
                            {
                                id: '5',
                                name: 'No contesta/ cliente falso'
                            },
                            {
                                id: '99',
                                name: 'Otros... Especificar'
                            }
                        ],
                        selectedOption: {
                            id: '3',
                            name: 'Option C'
                        } //This sets the default value of the select in the ui
                    };

                    $scope.ok = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                },
                resolve: {
                    row: function () {
                        return row;
                    }
                }
            });
        }



        $scope.gridOptions = {
            enableFiltering: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            },
            columnDefs: [{ name: 'Ver Solicitud', enableCellEdit: false, cellTemplate: require('./botonversolicitud.html') },
            {
                name: 'estado_solicitud', editableCellTemplate: 'ui-grid/dropdownEditor',
                cellFilter: 'mapEstado_solicitud', editDropdownValueLabel: 'estado', editDropdownOptionsArray: [
                    { id: 2, estado: 'Asignada' },
                    { id: 4, estado: 'Fallida' },
                    { id: 6, estado: 'Duplicada' },
                    { id: 5, estado: 'Eliminada' },
                    { id: 3, estado: 'Cerrada' },
                    { id: 98, estado: 'Cliente Falso' },
                    { id: 97, estado: 'Pruebas QP' },
                    { id: 99, estado: 'Otro' }
                ]
            },
            { name: 'nombrepersona', displayName: 'Nombre Persona', enableCellEdit: false },
            { name: 'rutpersona', displayName: 'Rut', enableCellEdit: false },
            { name: 'correopersona', displayName: 'Email', enableCellEdit: false },
            { name: 'telefonopersona', displayName: 'Telefono', enableCellEdit: false },
            { name: 'nombreplan', displayName: 'Codigo Plan', enableCellEdit: false },
            { name: 'precio_solicitado', displayName: 'Precio calculado', enableCellEdit: false },
            { name: 'fecha', displayName: 'Fecha Solicitud', enableCellEdit: false },
            ]


        }
    }
}

export default angular.module('sociosqueplanApp.missolicitudes', [uiRouter])
    .config(routes)
    .component('missolicitudes', {
        template: require('./missolicitudes.html'),
        controller: MissolicitudesComponent,
        controllerAs: 'missolicitudesCtrl'
    })
    .component('verunasolicitud', {
        template: require('./unasolicitud.html'),
        bindings: {
            entityfila: '<',
        }
    })
    .name;
