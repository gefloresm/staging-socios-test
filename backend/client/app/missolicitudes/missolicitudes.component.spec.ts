'use strict';

describe('Component: MissolicitudesComponent', function() {
  // load the controller's module
  beforeEach(module('sociosqueplanApp.missolicitudes'));

  var MissolicitudesComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    MissolicitudesComponent = $componentController('missolicitudes', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
