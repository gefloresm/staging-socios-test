'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('missolicitudes', {
      url: '/missolicitudes',
      template: '<missolicitudes></missolicitudes>'
    });
}
