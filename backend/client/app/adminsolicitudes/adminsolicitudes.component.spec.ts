'use strict';

describe('Component: AdminsolicitudesComponent', function() {
  // load the controller's module
  beforeEach(module('sociosqueplanApp.adminsolicitudes'));

  var AdminsolicitudesComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminsolicitudesComponent = $componentController('adminsolicitudes', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
