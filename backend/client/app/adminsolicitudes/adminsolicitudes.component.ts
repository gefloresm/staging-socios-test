'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './adminsolicitudes.routes';

export class AdminsolicitudesComponent {
    /*@ngInject*/
    constructor($scope, $http, uiGridConstants, $filter, $uibModal) {
        'ngInject';

        // -----------  ASIGNACION AUTOMATICA [ON/OFF]---------
        $scope.claseAsignacionAutomatica = "btn btn-default";

        $http.get('/api/configuracion/consultar')
            .then(function (data) {
                if (data.data.option_value == "true") {
                    $scope.claseAsignacionAutomatica = "btn btn-success";
                } else {
                    $scope.claseAsignacionAutomatica = "btn btn-default";
                }
            });

        $scope.activarAsignacionAutomatica = function () {
            $http.get('/api/configuracion/modificar')
                .then(function (data) {
                    if (data.data.option_value == "true") {
                        $scope.claseAsignacionAutomatica = "btn btn-success";
                    } else {
                        $scope.claseAsignacionAutomatica = "btn btn-default";
                    }
                });
        }
        // -----------  ASIGNACION AUTOMATICA [ON/OFF] ---------

        $scope.abrirmodalsolicitud = function (row) {
            $uibModal.open({
                animation: true,
                template: require('./modalAsignarSolicitud.html'),
                size: 'lg',
                controller: function ($scope, $uibModalInstance, row, Auth) {
                    // var $ctrl = this;
                    $scope.entityfila = angular.copy(row.entity);
                    $scope.entityfila.privilegiosUsuario = Auth.getCurrentUserSync().role;
                    $scope.emailasign = {};
                    $scope.emailalClienteDesdeModal = function () {
                        $http.post('/api/solicitarplans/emailasignacion', $scope.emailasign)
                            .then(function (response) {
                                $scope.mensajeasginaremail = response.data;
                            }).catch(function (e) {
                                $scope.errorasginaremail = 'Error en email de cliente, esto no debería pasar. Avisar';
                                $scope.mensajeasginaremail = e;
                                throw e;
                            });
                    }

                    $scope.emailalSocioDesdeModal = function () {
                        $http.post('/api/solicitarplans/socioemailasignacion', $scope.emailasign)
                            .then(function (response) {
                                $scope.sociomensajeasginaremail = response.data;
                                row.entity.estado_solicitud = 2;
                                row.entity.asignado_a = $scope.emailasign.id_socio;
                                $http.patch('/api/solicitarplans/' + row.entity._id, row.entity).then(function (response) {
                                    $scope.cambioDeEstadoCerrado = response;
                                    $http.post('/api/notificacion/prospecto-nuevo', { id_socio: row.entity.asignado_a })
                                        .then(function (response) { console.log("Envio de notificacion 'Prospecto nuevo'"); })
                                        .catch(function (err) { console.log("Error envio de notificacion 'Prospecto nuevo'"); })
                                }).catch(function (e) {
                                    alert("Error al cambiar estado a Cerrado. Se tiene que cambiar el estado. El error es el siguiente:" + JSON.stringify(e));
                                    throw e;
                                });
                            }).catch(function (e) {
                                $scope.socioerrorasginaremail = 'Error al enviar email a socio, esto no debería pasar. Avisar';
                                $scope.sociomensajeasginaremail = e;
                                throw e;
                            });
                    }

                    console.log(row);
                    var regionFiltroAsignacion = null;
                    if ($scope.entityfila.region) {
                        regionFiltroAsignacion = $scope.entityfila.region.numero;
                    }

                    $http.post('/api/administrarsocioss/asignar', { isapre: $scope.entityfila.codigoisapre, region: regionFiltroAsignacion })
                        .then(function (data) {
                            $scope.datossocios = data.data.filter(function (socio) {
                                return ((socio.isapre === $scope.entityfila.codigoisapre) && (socio.estado === true) && (socio.tmp_suspendido === false));
                            });
                        });

                    $scope.asignarSolicitud = function (id_socio) {
                        $scope.emailasign.id_solicitud = $scope.entityfila._id;
                        $scope.emailasign.id_socio = id_socio;
                        $scope.emailalClienteDesdeModal();
                        $scope.emailalSocioDesdeModal();

                        $scope.asignado = true;
                    };

                    $scope.cerrarModal = function () {
                        $uibModalInstance.dismiss();
                    };
                },
                resolve: {
                    row: function () {
                        return row;
                    }
                }
            });
        };

        $scope.modalCierreSolicitud = function (row) {
            var modalInstance = $uibModal.open({
                animation: true,
                template: require('./modalCierreSolicitud.html'),
                size: 'lg',
                controller: function ($scope, $uibModalInstance, row, $rootScope) {
                    // var $ctrl = this;
                    $scope.entityfila = angular.copy(row.entity);
                    console.log(row);

                    $scope.UF = $rootScope.UF;

                    $scope.calcularaCobrar = function () {
                        if ($scope.UF.valor > 0 && $scope.entityfila.precio_solicitado > 0 && $scope.datosSocio.tarifa > 0) { }
                        $scope.aCobrar = Math.round($scope.UF.valor * $scope.precio * ($scope.comision / 100));
                    }

                    if ($scope.entityfila.asignado_a !== null) {
                        $http.get('/api/administrarsocioss')
                            .then(function (data) {
                                $scope.datosSocio = data.data.filter(function (socio) {
                                    return socio._id === $scope.entityfila.asignado_a;
                                })[0];
                                console.log($scope.datosSocio);
                                $scope.precio = angular.copy(row.entity.precio_solicitado);
                                $scope.comision = angular.copy($scope.datosSocio.tarifa);
                                $scope.calcularaCobrar();
                            });
                    }

                    $scope.cierreSolicitud = function () {
                        const cierreSolicitud = {
                            id_socio: $scope.datosSocio._id,
                            monto: parseInt($scope.aCobrar) * -1,
                            tipo_de_movimiento: 101,
                            id_de_solicitud: $scope.entityfila._id,
                            descripcion: $scope.descripcion
                        };

                        $http.post('/api/movimientosycuentas', cierreSolicitud)
                            .then(function (response) {
                                $scope.mensajedescontar = response.data;
                                row.entity.estado_solicitud = 3;
                                $uibModalInstance.close(row);
                                alert("Sin errores. Respuesta:" + JSON.stringify($scope.mensajedescontar));
                                // Notificar si el saldo es menor a 80.000
                                if (cierreSolicitud.monto < 80000 && $scope.datosSocio.isapre != 78) {
                                    $http.post('/api/notificacion/bajo-saldo', cierreSolicitud)
                                        .then(function (response) {
                                            console.log("Respuesta de notificacion: ", response);
                                        })
                                        .catch(function (err) {
                                            console.log("Error de notificacion: ", err);
                                        })
                                }
                            }).catch(function (e) {
                                $scope.errorendescontar = 'Error, esto no debería pasar. Avisar';
                                $scope.mensajedescontar = e;
                                alert("Error, favor notificar:" + JSON.stringify($scope.mensajedescontar));
                                throw e;
                            });
                    };

                    $scope.cerrarModal = function () {
                        $uibModalInstance.dismiss();
                    };
                },
                resolve: {
                    row: function () {
                        return row;
                    }
                }
            });

            modalInstance.result.then(function (row) {
                $http.patch('/api/solicitarplans/' + row.entity._id, row.entity).then(function (response) {
                }).catch(function (e) {
                    alert("Error al cambiar estado a Cerrado. Se tiene que cambiar el estado. El error es el siguiente:" + JSON.stringify(e));
                    throw e;
                });
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.fechahoy = new Date();

        $scope.emailalCliente = function () {
            $http.post('/api/solicitarplans/emailasignacion', $scope.emailasign)
                .then(function (response) {
                    $scope.mensajeasginaremail = response.data;
                }).catch(function (e) {
                    $scope.errorasginaremail = 'Error en email de cliente, esto no debería pasar. Avisar';
                    $scope.mensajeasginaremail = e;
                    throw e;
                });
        }

        $scope.emailalSocio = function () {
            $http.post('/api/solicitarplans/socioemailasignacion', $scope.emailasign)
                .then(function (response) {
                    $scope.sociomensajeasginaremail = response.data;
                }).catch(function (e) {
                    $scope.socioerrorasginaremail = 'Error en email de socio, esto no debería pasar. Avisar';
                    $scope.sociomensajeasginaremail = e;
                    throw e;
                });
        }

        $scope.emailAsignacion = function () {
            $scope.emailalCliente();
            $scope.emailalSocio();
        }

        $http.get('/api/solicitarplans')
            .then(function (data) {
                data.data.map(function (elemento) {
                    elemento.diassolicitud = Math.round(100 * ($scope.fechahoy - +new Date(elemento.fecha)) / (1000 * 60 * 60 * 24)) / 100;
                    if (elemento.fecha_asignado !== null) {
                        elemento.horas_para_asignar = Math.round(100 * (+new Date(elemento.fecha_asignado) - +new Date(elemento.fecha)) / (1000 * 60 * 60)) / 100;
                    }
                    //Linea para saber si hay cambio en la fecha de asignado
                    elemento.asignadoanterior = elemento.asignado_a;
                    return elemento;
                })
                $scope.gridOptions.data = data.data;
            })

        $scope.gridOptions = {
            enableFiltering: true,
            enableSorting: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
            },
            columnDefs: [{
                name: 'Ver Solicitud',
                enableCellEdit: false,
                cellTemplate: require('../missolicitudes/botonversolicitud.html')
            }, {
                name: 'Cerrar Solicitud',
                enableCellEdit: false,
                cellTemplate: `<div class="ui-grid-cell-contents">
                <button type="button"
                   class="btn btn-xs btn-success"
                   ng-click="grid.appScope.modalCierreSolicitud(row)">
                  <i class="fa fa-money"></i>
                </button>
              </div>`
            }, {
                name: '_id',
                enableCellEdit: false,
                width: 60
            },
            {
                name: 'estado_solicitud',
                editableCellTemplate: 'ui-grid/dropdownEditor',
                width: 100,
                cellFilter: 'mapEstado_solicitud',
                cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                    if (grid.getCellValue(row, col) == 2) {
                        return 'amarilloasig';
                    } else if (grid.getCellValue(row, col) == 3) {
                        return 'verdeasig';
                    } else if (grid.getCellValue(row, col) == 4 || grid.getCellValue(row, col) == 5) {
                        return 'rojoasig';
                    } else {
                        return 'azulasig';
                    }
                },
                editDropdownValueLabel: 'estado',
                editDropdownOptionsArray: [
                    {
                        id: 2,
                        estado: 'Asignada'
                    },
                    {
                        id: 4,
                        estado: 'Fallida'
                    },
                    {
                        id: 6,
                        estado: 'Duplicada'
                    },
                    {
                        id: 5,
                        estado: 'Eliminada'
                    },
                    {
                        id: 3,
                        estado: 'Cerrada'
                    },
                    {
                        id: 98,
                        estado: 'Cliente Falso'
                    },
                    {
                        id: 97,
                        estado: 'Pruebas QP'
                    },
                    {
                        id: 99,
                        estado: 'Otro'
                    }
                ],
                filter: {
                    type: uiGridConstants.filter.SELECT,
                    selectOptions: [{
                        value: 2,
                        label: 'Asignada'
                    },
                    {
                        value: 4,
                        label: 'Fallida'
                    },
                    {
                        value: 6,
                        label: 'Duplicada'
                    },
                    {
                        value: 5,
                        label: 'Eliminada'
                    },
                    {
                        value: 3,
                        label: 'Cerrada'
                    },
                    {
                        value: 98,
                        label: 'Cliente Falso'
                    },
                    {
                        value: 97,
                        label: 'Pruebas QP'
                    },
                    {
                        value: 99,
                        label: 'Otro'
                    }]
                }
            },
            {
                name: 'razon_fallo',
                editableCellTemplate: 'ui-grid/dropdownEditor',
                width: 120,
                cellFilter: 'mapRazon_fallo',
                editDropdownValueLabel: 'razon',
                editDropdownOptionsArray: [
                    {
                        id: 1,
                        razon: 'Pre Existencias'
                    },
                    {
                        id: 2,
                        razon: 'Menos de 1 año'
                    },
                    {
                        id: 3,
                        razon: 'No contesta'
                    },
                    {
                        id: 4,
                        razon: 'Cerró con otra isapre'
                    },
                    {
                        id: 5,
                        razon: 'Cerró con otro vendedor'
                    },
                    {
                        id: 6,
                        razon: 'Misma isapre'
                    },
                    {
                        id: 7,
                        razon: 'No cumplió exámenes'
                    },
                    {
                        id: 9,
                        razon: 'Región sin ejecutivo'
                    },
                    {
                        id: 10,
                        razon: 'Se quedó en su Isapre/Fonasa'
                    },
                    {
                        id: 11,
                        razon: 'No le gustó el plan'
                    },
                    {
                        id: 12,
                        razon: 'Solo cotizaba'
                    },
                    {
                        id: 13,
                        razon: 'No acepta reunión'
                    },
                    {
                        id: 14,
                        razon: 'Lo verá más adelante'
                    },
                    {
                        id: 15,
                        razon: 'Fue directo a la Isapre /call center'
                    },
                    {
                        id: 16,
                        razon: 'Ya tenía ejecutivo'
                    },
                    {
                        id: 17,
                        razon: 'Licencia / Embarazo'
                    },
                    {
                        id: 18,
                        razon: 'Quedó cesante'
                    },
                    {
                        id: 19,
                        razon: 'Tiene deuda'
                    },
                    {
                        id: 20,
                        razon: 'Caro / Precio no corresponde (no puso bien los datos)'
                    },
                    {
                        id: 21,
                        razon: 'Datos de contacto erróneos (Mail / Teléfono malo)'
                    },
                    {
                        id: 8,
                        razon: 'Otros (especificar nota)'
                    }

                ],
                filter: {
                    type: uiGridConstants.filter.SELECT,
                    selectOptions: [
                        { value: 1, label: 'Pre Existencias' },
                        { value: 2, label: 'Menos de 1 año' },
                        { value: 3, label: 'No contesta' },
                        { value: 4, label: 'Cerró con otra isapre' },
                        { value: 5, label: 'Cerró con otro vendedor' },
                        { value: 6, label: 'Misma isapre' },
                        { value: 7, label: 'No cumplió exámenes' },
                        { value: 9, label: 'Región sin ejecutivo' },
                        { value: 10, label: 'Se quedó en su Isapre/Fonasa' },
                        { value: 11, label: 'No le gustó el plan' },
                        { value: 12, label: 'Solo cotizaba' },
                        { value: 13, label: 'No acepta reunión' },
                        { value: 14, label: 'Lo verá más adelante' },
                        { value: 15, label: 'Fue directo a la Isapre /call center' },
                        { value: 16, label: 'Ya tenía ejecutivo' },
                        { value: 17, label: 'Licencia / Embarazo' },
                        { value: 18, label: 'Quedó cesante' },
                        { value: 19, label: 'Tiene deuda' },
                        { value: 20, label: 'Caro / Precio no corresponde (no puso bien los datos)' },
                        { value: 21, label: 'Datos de contacto erróneos (Mail / Teléfono malo)' },
                        { value: 8, label: 'Otros (especificar nota)' },
                    ]
                }
            },
            {
                name: 'Dias_solicitud',
                width: 80,
                field: 'diassolicitud',
                enableCellEdit: false
            },
            {
                name: 'Horas_hasta_asignar',
                width: 80,
                field: 'horas_para_asignar',
                enableCellEdit: false
            },
            {
                name: 'asignado_por',
                type: 'number',
                width: 60,
                filter: {
                    condition: uiGridConstants.filter.EXACT
                }
            },
            {
                name: 'notas_admin',
                width: 200
            },
            {
                name: 'asignado_a',
                type: 'number',
                width: 60,
                filter: {
                    condition: uiGridConstants.filter.EXACT
                }
            },
            {
                name: 'es_de_la_misma_isapre',
                type: 'boolean',
                enableCellEdit: false,
                width: 80
            },
            {
                name: 'email_misma_isapre',
                type: 'boolean',
                enableCellEdit: false,
                width: 80
            },
            {
                name: 'nombrepersona',
                enableCellEdit: false,
                width: 200
            },
            {
                name: 'rutpersona',
                enableCellEdit: false
            },
            {
                name: 'correopersona',
                enableCellEdit: false,
                width: 200
            },
            {
                name: 'telefonopersona',
                enableCellEdit: false,
                width: 100
            },
            {
                name: 'Código Plan',
                field: 'nombreplan',
                enableCellEdit: false,
                width: 120
            },
            {
                name: 'codigoisapre',
                enableCellEdit: false,
                width: 70,
                filter: {
                    condition: uiGridConstants.filter.EXACT
                }
            },
            {
                name: 'precio_solicitado',
                enableCellEdit: false,
                width: 90
            },
            {
                name: 'fecha',
                enableCellEdit: false,
                type: 'date',
                cellFilter: 'date:"short"',
                width: 120
            },
            {
                name: 'emailsocio',
                enableCellEdit: false
            },
            {
                name: 'emailcliente',
                enableCellEdit: false
            }
            ]
        }

        var promesaactualizar = function (row) {
            return $http.patch('/api/solicitarplans/' + row._id, row);
        }

        $scope.saveRow = function (rowEntity) {
            //verifica si hay cambios en asignar ejecutivo
            if (rowEntity.asignadoanterior !== rowEntity.asignado_a) {
                rowEntity.cambiosenasignarejec = true;
            } else {
                rowEntity.cambiosenasignarejec = false;
            }
            console.log(rowEntity);
            $scope.gridApi.rowEdit.setSavePromise(rowEntity, promesaactualizar(rowEntity));
        };

    }
}
/*{
  "_id": 3,
  "nombrepersona": "Matias Stager u",
  "rutpersona": "1231",
  "correopersona": "solicitudes.queplan@gmail.com",
  "telefonopersona": "987697200",
  "nombreplan": "JEM14",
  "codigoisapre": 88,
  "datospersona": {
    "cotizante": [
      {
        "sexo": "Masculino",
        "edad": 30,
        "ingreso": 750000
      },
      {
        "sexo": "Femenino",
        "ingreso": null,
        "edad": 30
      }
    ],
    "cargas": [
      {
        "sexo": "Masculino",
        "edad": 2
      },
      {
        "sexo": "Masculino",
        "edad": 5
      }
    ],
    "region": {
      "value": {
        "orden": 2,
        "abv": "XV",
        "nombre": "Arica y Parinacota",
        "numero": 15
      }
    },
    "soloopareja": "solo",
    "ingresodatos": true
  },
  "desde": "comparar",
  "ip": "127.0.0.1",
  "fecha": "2017-03-11T01:51:21.000Z",
  "emailsocio": "Email cliente enviado: 250 Message received",
  "emailcliente": "Email cliente enviado: 250 Message received",
  "asignado_a": null,
  "estado_solicitud": null,
  "notas_socio": null,
  "notas_admin": null,
  "fecha_asignado": null,
  "ultimo_estado_socio": null,
  "ultimo_estado_admin": null,
  "$$hashKey": "uiGrid-0006"
}*/

export default angular.module('sociosqueplanApp.adminsolicitudes', [uiRouter])
    .config(routes)
    .component('adminsolicitudes', {
        template: require('./adminsolicitudes.html'),
        controller: AdminsolicitudesComponent,
        controllerAs: 'adminsolicitudesCtrl'
    }).filter('mapEstado_solicitud', function () {
        var estado_solicitudHash = {
            2: 'Asignada',
            4: 'Fallida',
            6: 'Duplicada',
            5: 'Eliminada',
            3: 'Cerrada',
            98: 'Cliente Falso',
            97: 'Pruebas QP',
            99: 'Otro'
        };
        return function (input) {
            if (!input) {
                return '';
            } else {
                return estado_solicitudHash[input];
            }
        };
    }).filter('mapRazon_fallo', function () {
        var razon_falloHash = {
            1: 'Pre Existencias',
            2: 'Menos de 1 año',
            3: 'No contesta',
            4: 'Cerró con otra isapre',
            5: 'Cerró con otro vendedor',
            6: 'Misma isapre',
            7: 'No cumplió exámenes',
            8: 'Otros (especificar nota)',
            9: 'Región sin ejecutivo',
            10: 'Se quedó en su Isapre/Fonasa',
            11: 'No le gustó el plan',
            12: 'Solo cotizaba',
            13: 'No acepta reunión',
            14: 'Lo verá más adelante',
            15: 'Fue directo a la Isapre /call center',
            16: 'Ya tenía ejecutivo',
            17: 'Licencia / Embarazo',
            18: 'Quedó cesante',
            19: 'Tiene deuda',
            20: 'Caro / Precio no corresponde (no puso bien los datos)',
            21: 'Datos de contacto erróneos (Mail / Teléfono malo)'
        };
        return function (input) {
            if (!input) {
                return '';
            } else {
                return razon_falloHash[input];
            }
        };
    })
    .name;