'use strict';
const angular = require('angular');
// import ngAnimate from 'angular-animate';
const ngCookies = require('angular-cookies');
const ngResource = require('angular-resource');
const ngSanitize = require('angular-sanitize');
import 'angular-socket-io';
const ngTouch = require('angular-touch');

const uiRouter = require('angular-ui-router');
const uiBootstrap = require('angular-ui-bootstrap');
// import ngMessages from 'angular-messages';
import 'angular-validation-match';


import {routeConfig} from './app.config';

import _Auth from '../components/auth/auth.module';
import account from './account';
import admin from './admin';
import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import constants from './app.constants';
import util from '../components/util/util.module';
import socket from '../components/socket/socket.service';

//Adicionales
import 'angular-ui-grid';
import './i18n/angular-locale_es-cl';

import './app.scss';

import AdminsolicitudesComponent from './adminsolicitudes/adminsolicitudes.component';
import AdminsociosComponent from './adminsocios/adminsocios.component';
import MissolicitudesComponent from './missolicitudes/missolicitudes.component';
import Adminsaldosymovimientos from './adminsaldosymovimientos/adminsaldosymovimientos.component';
import Saldosymovimientos from './saldosymovimientos/saldosymovimientos.component';
import AdmincontactosComponent from './admincontactos/admincontactos.component';

angular.module('sociosqueplanApp', [ngCookies, ngResource, ngSanitize, 'btford.socket-io', uiRouter,
  uiBootstrap, _Auth, account, admin, 'validation.match', navbar, footer, main, constants, socket, util, AdminsolicitudesComponent, AdminsociosComponent, MissolicitudesComponent, Adminsaldosymovimientos, Saldosymovimientos, AdmincontactosComponent, ngTouch, 'ui.grid', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.cellNav', 'ui.grid.resizeColumns', 'ui.grid.moveColumns'
])
  .config(routeConfig)
  .run(function ($rootScope, $location, Auth, $http) {
    'ngInject';
    // Redirect to login if route requires auth and you're not logged in
    /*     var url1 = './missolicitudes/unasolicitud.html';
        $templateCache.put(url1, require(url1)); */



    $http.get('/api/recursos/uf')
      .then(function (res) {
        // Cambiar
        $rootScope.UF = { "valor": 26635.35, "dia": false };
        console.log(res.data);
        $rootScope.UF = res.data;
        /*         $rootScope.valoruf = Number(res.data.UFs[0].Valor.replace(".", "").replace(",", "."))
                console.log("Valor de la UF: " + $rootScope.valoruf); */
      });
    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$stateChangeStart', function(event, next) {
      Auth.isLoggedIn(function(loggedIn) {
        if(next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  });

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['sociosqueplanApp'], {
      strictDi: true
    });
  });
