'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('saldosymovimientos', {
      url: '/saldosymovimientos',
      template: '<saldosymovimientos></saldosymovimientos>'
    });
}
