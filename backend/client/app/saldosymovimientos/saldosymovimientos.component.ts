'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './saldosymovimientos.routes';

export class SaldosymovimientosComponent {
    /*@ngInject*/
    constructor($scope, $http, $uibModal) {
        'ngInject';
        
               $scope.abrirmodalsolicitud = function (grid, row) {
           $uibModal.open({
               animation: true,
               template: require('../missolicitudes/vermodalsolicitud.html'),
               size: 'lg',
               controller: function ($scope, $uibModalInstance, grid, row) {
                   // var $ctrl = this;
                   $scope.entityfila = angular.copy(row.entity.solicitud[0]);
                   console.log(grid);
                   console.log(row);

                   $scope.ok = function () {
                       $uibModalInstance.close();
                   };
               },
               resolve: {
                   grid: function () {
                       return grid;
                   },
                   row: function () {
                       return row;
                   }
               }
           });
       }
        
        $http.get('/api/movimientosycuentas/misaldo')
            .then(function (data) {
                $scope.misaldo = data.data[0].saldo;
            console.log($scope.misaldo);
            });
        $http.get('/api/movimientosycuentas/mismovimientos')
            .then(function (data) {
                $scope.mismovimientos.data = data.data;
            });
        
              $scope.mismovimientos = {
          enableFiltering: true,
          columnDefs: [
              { name: 'Ver', enableCellEdit: false, cellTemplate: require('../missolicitudes/botonversolicitud.html'), width: 80},
              { field: '_id', enableCellEdit: false, width: '10%' },
              { name: 'Nombre', field: 'solicitud[0].nombrepersona', enableCellEdit: false, width: 200 },
           //  { field: 'id_socio', enableCellEdit: false},
              { field: 'monto', enableCellEdit: false, cellFilter: 'currency:"$":0'},
              { field: 'tipo_de_movimiento', enableCellEdit: false, cellFilter: 'tipoDeMovimiento'},
              { field: 'descripcion', enableCellEdit: false},
           //   { field: 'id_de_solicitud', enableCellEdit: false},
              { field: 'fecha_movimiento', enableCellEdit: false}
          ]
      }
    }
}

export default angular.module('sociosqueplanApp.saldosymovimientos', [uiRouter])
    .config(routes)
    .component('saldosymovimientos', {
        template: require('./saldosymovimientos.html'),
        controller: SaldosymovimientosComponent,
        controllerAs: 'saldosymovimientosCtrl'
    })
    .name;