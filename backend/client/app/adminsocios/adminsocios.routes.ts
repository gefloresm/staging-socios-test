'use strict';

export default function ($stateProvider) {
    'ngInject';
    $stateProvider
        .state('adminsocios', {
            url: '/adminsocios',
            template: '<adminsocios></adminsocios>',
            authenticate: 'admin'
        })
        .state('sociosCB', {
            url: '/sociosCB',
            template: '<adminsocios></adminsocios>',
            authenticate: 'cb'
        });;
}