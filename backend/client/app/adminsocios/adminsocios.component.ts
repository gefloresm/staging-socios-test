'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './adminsocios.routes';

export class AdminsociosComponent {
    /*@ngInject*/
    constructor($scope, $http, uiGridConstants) {
        'ngInject';

        $http.get('/api/administrarsocioss')
            .then(function (data) {
                $scope.gridsocios.data = data.data;
                console.log($scope.gridsocios);
            });

        $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
            if (col.filters[0].term) {
                return 'header-filtered';
            } else {
                return '';
            }
        };

        var promesaactualizar = function (row) {
            if (row.region_secundaria === "") {
                row.region_secundaria = null;
            }
            return $http.patch('/api/administrarsocioss/' + row._id, row);
        }

        $scope.saveRow = function (rowEntity) {
            console.log(rowEntity);
            $scope.gridApi.rowEdit.setSavePromise(rowEntity, promesaactualizar(rowEntity));
        };
        $scope.gridsocios = {
            enableFiltering: true,
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
            },
            columnDefs: [
                { field: '_id', enableCellEdit: false },
                { field: 'id_de_usuario' },
                { field: 'nombres' },
                { field: 'apellidos' },
                { field: 'rut_socio' },
                { field: 'isapre' },
                { field: 'estado' },
                { field: 'tmp_suspendido' },
                { field: 'registro_en_bd' },
                { field: 'telefono_1' },
                { field: 'telefono_2' },
                { field: 'email_corporativo' },
                { field: 'email_personal' },
                { field: 'correo_preferencia' },
                { field: 'ciudad' },
                { field: 'region_principal' },
                { field: 'region_secundaria' },
                { field: 'direccion_personal' },
                { field: 'tarifa' },
                { field: 'tarifa_fija_por_envio' },
                { field: 'especial_alta_uf', type: 'boolean' },
                { field: 'saldo', enableCellEdit: false }
            ]
        }

        /*{
          "registro_en_bd": "2017-04-01T19:18:27.000Z",
          "_id": 12,
          "nombres": "Nombres",
          "apellidos": "Apellido",
          "rut_socio": "rut",
          "isapre": 1,
          "estado": true,
          "telefono_1": "123123",
          "telefono_2": null,
          "email_corporativo": "email corporativo",
          "email_personal": "email personal",
          "correo_preferencia": "",
          "ciudad": "Ciudad",
          "region_principal": 13,
          "region_secundaria": null,
          "direccion_personal": "Direccion",
          "saldo": 0,
          "id_de_usuario": null,
          "$$hashKey": "uiGrid-00DE"
        }*/

        $scope.addData = function () {
            var n = $scope.gridsocios.data.length + 1;

            $http.post('/api/administrarsocioss', {
                "nombres": "Nombres",
                "apellidos": "Apellido",
                "rut_socio": "rut",
                "isapre": 1,
                "estado": true,
                "telefono_1": "123123",
                "telefono_2": null,
                "email_corporativo": "email corporativo",
                "email_personal": "email personal",
                "correo_preferencia": "",
                "ciudad": "Ciudad",
                "region_principal": 13,
                "region_secundaria": null,
                "direccion_personal": "Direccion",
                "saldo": 0,
                "tarifa": 0,
                "tarifa_fija_por_envio": 0
            }).then(function (data) {
                console.log(data);
                $scope.gridsocios.data.push(data.data);
            });

        };

    }
}

export default angular.module('sociosqueplanApp.adminsocios', [uiRouter])
    .config(routes)
    .component('adminsocios', {
        template: require('./adminsocios.html'),
        controller: AdminsociosComponent,
        controllerAs: 'adminsociosCtrl'
    })
    .name;