'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('adminsaldosymovimientos', {
      url: '/adminsaldosymovimientos',
      template: '<adminsaldosymovimientos></adminsaldosymovimientos>'
    });
}
