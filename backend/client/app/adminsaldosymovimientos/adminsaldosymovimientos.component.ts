'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './adminsaldosymovimientos.routes';

export class AdminsaldosymovimientosComponent {

    /*@ngInject*/
    constructor($http, $scope, uiGridConstants) {
        'ngInject';
        $scope.anadir = {};
        $scope.mensajesumar = '';
        $scope.errorensumar = '';
        $scope.mensajedescontar = '';
        $scope.errorendescontar = '';

        $scope.datossocios = {};

        var sumarsaldos = function (socios) {
            var saldosumados = 0;
            for (var i = 0; i < socios.length; i++) {
                saldosumados += socios[i].saldo;
            }
            $scope.sumadesaldos = saldosumados;
        };


        $scope.cargarsocios = function () {
            $http.get('/api/administrarsocioss')
                .then(function (data) {
                    // Se filtran usuarios que son de Cruzblanca
                    $scope.datossocios = data.data.filter(socio => socio.isapre !== 78);
                    sumarsaldos($scope.datossocios);
                });
        };

        $scope.cargarmovimientos = function () {
            $http.get('/api/movimientosycuentas')
                .then(function (data) {
                    // La razón de esto es que el 1 se confunde con 11 y 12 y otros. El 1 se cambia a 1001
                    data.data.map(function (elemento) {
                        if (elemento.tipo_de_movimiento === 1) {
                            elemento.tipo_de_movimiento = 1001;
                        }
                    })
                    $scope.ultimosmovimientos.data = data.data;
                    console.log(data);
                });
        };

        $scope.cargarmovimientos();
        $scope.cargarsocios();

        $scope.sumarDinero = function () {
            console.log("Lamando a sumar dinero");
            $scope.anadir.tipo_de_movimiento = $scope.opcionessumar.opcionelegida.id;
            $http.post('/api/movimientosycuentas', $scope.anadir)
                .then(function (response) {
                    $scope.mensajesumar = response.data;
                    $scope.cargarsocios();
                    $scope.cargarmovimientos();
                }).catch(function (e) {
                    $scope.errorensumar = 'Error, esto no debería pasar. Avisar';
                    $scope.mensajesumar = e;
                    throw e;
                });
        }

        $scope.opcionessumar = {
            opcionesdisponibles: [
                {
                    id: '1',
                    name: 'Depósito Cuenta Corriente'
                },
                {
                    id: '2',
                    name: 'Otros Depósitos'
                },
                {
                    id: '11',
                    name: 'Bono de Ingreso'
                },
                {
                    id: '12',
                    name: 'Otros Bonos'
                },
                {
                    id: '13',
                    name: 'Reverso de cobro'
                },
                {
                    id: '99',
                    name: 'Otros Ingresos'
                }
            ],
            opcionelegida: {
                id: '1',
                name: 'Depósito Cuenta Corriente'
            } //This sets the default value of the select in the ui
        };

        $scope.descontarDinero = function () {
            console.log("Lamando a descontar dinero");
            $scope.descontar.tipo_de_movimiento = $scope.opcionesdescontar.opcionelegida.id;
            var descontar = angular.copy($scope.descontar);
            console.log(descontar);
            descontar.monto *= -1;
            $http.post('/api/movimientosycuentas', descontar)
                .then(function (response) {
                    $scope.mensajedescontar = response.data;
                    $scope.cargarsocios();
                    $scope.cargarmovimientos();
                }).catch(function (e) {
                    $scope.errorendescontar = 'Error, esto no debería pasar. Avisar';
                    $scope.mensajedescontar = e;
                    throw e;;
                });
        }
        $scope.opcionesdescontar = {
            opcionesdisponibles: [
                {
                    id: '101',
                    name: 'Cobro por prospecto cerrado'
                },
                {
                    id: '102',
                    name: 'Retiro de dinero'
                },
                {
                    id: '103',
                    name: 'Cobro por envío'
                },
                {
                    id: '199',
                    name: 'Otros Cobros'
                }
            ],
            opcionelegida: {
                id: '101',
                name: 'Cobro por prospecto cerrado'
            } //This sets the default value of the select in the ui
        };


        $scope.ultimosmovimientos = {
            enableFiltering: true,
            columnDefs: [
                {
                    field: '_id',
                    enableCellEdit: false
                },
                {
                    field: 'id_socio',
                    enableCellEdit: false,
                    filter: {
                        condition: uiGridConstants.filter.EXACT
                    }
                },
                {
                    field: 'monto',
                    enableCellEdit: false,
                    cellFilter: 'currency:"$":0'
                },
                {
                    field: 'tipo_de_movimiento',
                    enableCellEdit: false,
                    cellFilter: 'tipoDeMovimiento',
                    filter: {
                        type: uiGridConstants.filter.SELECT,
                        selectOptions: [{
                            value: 1001,
                            label: 'Depósito Cuenta Corriente'
                        },
                        {
                            value: 2,
                            label: 'Otros Depósitos'
                        },
                        {
                            value: 11,
                            label: 'Bono de Ingreso'
                        },
                        {
                            value: 12,
                            label: 'Otros Bonos'
                        },
                        {
                            value: 13,
                            label: 'Reverso de cobro'
                        },
                        {
                            value: 99,
                            label: 'Otros Ingresos'
                        },
                        {
                            value: 101,
                            label: 'Cobro por prospecto cerrado'
                        },
                        {
                            value: 102,
                            label: 'Retiro de dinero'
                        },
                        {
                            value: 103,
                            label: 'Cobro por envío'
                        },
                        {
                            value: 199,
                            label: 'Otros Cobros'
                        }]
                    }
                },
                {
                    field: 'descripcion',
                    enableCellEdit: false
                },
                {
                    field: 'id_de_solicitud',
                    enableCellEdit: false,
                    filter: {
                        condition: uiGridConstants.filter.EXACT
                    }
                },
                {
                    field: 'fecha_movimiento',
                    enableCellEdit: false,
                    type: 'date',
                    cellFilter: 'date:"short"',
                    width: 120
                }
            ]
        }

        /*        {{
          "_id": 22,
          "id_socio": 10,
          "monto": "30000",
          "tipo_de_movimiento": 1,
          "descripcion": "30 lucas",
          "id_de_solicitud": null,
          "fecha_movimiento": "2017-04-01T19:15:50.000Z",
          "originado_por_id": 999,
          "$$hashKey": "uiGrid-000C"
        }}*/
    }
}

export default angular.module('sociosqueplanApp.adminsaldosymovimientos', [uiRouter])
    .config(routes)
    .component('adminsaldosymovimientos', {
        template: require('./adminsaldosymovimientos.html'),
        controller: AdminsaldosymovimientosComponent,
        controllerAs: 'adminsaldosymovimientosCtrl'
    }).filter('tipoDeMovimiento', function () {
        var movimientoshash = {
            1001: 'Depósito Cuenta Corriente',
            2: 'Otros Depósitos',
            11: 'Bono de Ingreso',
            12: 'Otros Bonos',
            13: 'Reverso de cobro',
            99: 'Otros Ingresos',
            101: 'Cobro por prospecto cerrado',
            102: 'Retiro de dinero',
            103: 'Cobro por envío',
            199: 'Otros Cobros'
        };
        return function (input) {
            if (!input) {
                return '';
            } else {
                return movimientoshash[input];
            }
        };

    })
    .name;