'use strict';

describe('Component: AdminsaldosymovimientosComponent', function() {
  // load the controller's module
  beforeEach(module('sociosqueplanApp.adminsaldosymovimientos'));

  var AdminsaldosymovimientosComponent;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    AdminsaldosymovimientosComponent = $componentController('adminsaldosymovimientos', {});
  }));

  it('should ...', function() {
    expect(1).to.equal(1);
  });
});
