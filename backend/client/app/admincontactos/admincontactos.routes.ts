'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('admincontactos', {
      url: '/admincontactos',
      template: '<admincontactos></admincontactos>'
    });
}
